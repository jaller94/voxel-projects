const fill = (size, input, func) => {
  const data = [];
  let index = 0;
  for(let z = 0; z < size[2]; z++) {
    for(let y = 0; y < size[1]; y++) {
      for(let x = 0; x < size[0]; x++) {
        const value = func(x, y, z);
        data.push(value !== 0 ? value : input[index]);
        index++;
      }
    }
  }
  return data;
}

const fillCuboid = (size, input, cuboid, value = 0) => (
  fill(size, input, (x, y, z) => {
    const isPart = (
      x >= cuboid[0][0] && x <= cuboid[1][0] &&
      y >= cuboid[0][1] && y <= cuboid[1][1] &&
      z >= cuboid[0][2] && z <= cuboid[1][2]
    );
    return isPart ? value : 0;
  })
);

const compressData = (data) => {
  if (data.length === 0) return [];
  const compressed = [];
  let index = 1;
  let currentLength = 1;
  let currentValue = data[0];
  while(index < data.length) {
    if (currentValue !== data[index]) {
      if (currentLength > 1) {
        compressed.push(-currentLength);
      }
      compressed.push(currentValue);
      currentLength = 1;
      currentValue = data[index];
    } else {
      currentLength++;
    }
    index++;
  }
  if (currentLength > 1) {
    compressed.push(-currentLength);
  }
  compressed.push(currentValue);
  return compressed;
};

const mergeParts = (...parts) => {
  const data = [];
  for (let i = 0; i < parts[0].length; i++) {
    let value = 0;
    for (let layer = parts.length - 1; layer >= 0; layer--) {
      if (parts[layer][i] !== 0) {
        value = parts[layer][i];
        break;
      }
    }
    data.push(value);
  }
  return data;
}

const getIndexForPosition = (size, position) => {
  return position[0] % size[0] +
    (position[1] % size[1]) * size[0] + 
    position[2] * size[0] * size[1];
};

const setVoxel = (size, input, position, value) => {
  const index = getIndexForPosition(size, position);
  const data = uncompressData(input);
  if (index >= data.length) {
    throw new Error(`Position out of bounds: ${position[0]},${position[1]},${position[2]}`);
  }
  data[index] = value;
  return data;
};

const resizePart = (part, oldSize, size, translateX = 0, translateY = 0, translateZ = 0) => {
  const data = [];
  for(let z = 0; z < size[0]; z++) {
    for(let y = 0; y < size[1]; y++) {
      for(let x = 0; x < size[2]; x++) {
        
        const index = getIndexForPosition(oldSize, [
          x - translateX,
          y - translateY,
          z - translateZ
        ]);
        const includedInPart = (
          x - translateX >= 0 && x - translateX < oldSize[0] &&
          y - translateY >= 0 && y - translateY < oldSize[1] &&
          z - translateZ >= 0 && z - translateZ < oldSize[2]
        ); 
        data.push(includedInPart ? part[index] : 0);
      }
    }
  }
  return data;
}

const uncompressData = (data) => {
  const uncompressed = [];
  let index = 0;
  while(index < data.length) {
    if (data[index] < 0) {
      for(let i = 0; i > data[index]; i--) {
        uncompressed.push(data[index + 1]);
      }
      index += 2;
    } else {
      uncompressed.push(data[index]);
      index++;
    }
  }
  return uncompressed;
};

module.exports = {
  compressData,
  fillCuboid,
  getIndexForPosition,
  mergeParts,
  resizePart,
  setVoxel,
  uncompressData
};
