const fs = require('fs');
const {
  compressData,
  mergeParts,
  resizePart
} = require('./helpers');
const {
  generateDirt
} = require('./minecraft/dirt');
const {
  generateGrass
} = require('./minecraft/grass');

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16).toUpperCase();
  });
}

const randomInt = (min, max) => (
  Math.floor(Math.random() * (max - min + 1)) + min
);

const worldSize = [128, 128, 128];

const groundSize = [worldSize[0], worldSize[1], 16];
let data = generateGrass(groundSize);
// data = resizePart(data, groundSize, worldSize);

const blocks = [];
const blockSize = [16, 16, 16];

const addPart = (data) => {
  blocks.push(data);
}

for(let y = 1; y < 7; y++) {
  for(let x = 1; x < 7; x++) {
    const height = randomInt(-2, 4);
    if (height >= 1) {
      const partSize = [16, 16, 16*height];
      let grassBlock = generateGrass(partSize);
      grassBlock = resizePart(grassBlock, partSize, worldSize, x*16, y*16, 16);
      addPart(grassBlock);
    }
    // if (height === 2) {
    //   let grassBlock = generateGrass(blockSize);
    //   grassBlock = resizePart(grassBlock, blockSize, worldSize, x*16, y*16, 32);
    //   addPart(grassBlock);
    //   let dirtBlock = generateDirt(blockSize);
    //   dirtBlock = resizePart(dirtBlock, blockSize, worldSize, x*16, y*16, 16);
    //   addPart(dirtBlock);
    // }
  }
}

const model = {
  formatVersion: 2,
  fileType: "SpriteStackModel",
  parts: [
    {
      name: `ground`,
      bounds: [64,72,55,63,0,1],
      size: groundSize,
      data: compressData(data),
      uuid: uuidv4(),
      hidden: false
    },
    {
      name: `blocks`,
      bounds: [64,72,55,63,0,1],
      size: worldSize,
      data: compressData(mergeParts(...blocks)),
      uuid: uuidv4(),
      hidden: false
    }
  ],
  size: worldSize,
  palette: [5670729,7581794,5539144,5011776,5407046,4747836,5275204,6000205,5670730,5143619,5011777,5077569,6395474,4023092,5275461,8224125,7500402,9276813,6645093,12026713,9726278,7819572,5650465,4879677,12228191,10322504,11701847,6131790,5867851,4748093,5867596,6461524,6329938,5209412,8750469,5802058,7428914,6198096,4681788,5407302,6065998,6659159,4418104,6988891,6922842,5011262,4417848,5011520,5538630,5538888,4945469,6908265,6705195,6197840,6593366,7120989,4286520,6593110,4682044,4483640,5933900,5341254,5736009,5999692,6329680,8289918,5999949,4615996,6263888,4879421,6527317,7450207,6989147,4549178,5143363,4615227,4549690,6461268,6132047,6724951,7492926,5802059,7384416,8019770,4732958],
  bounds: [64,72,55,63,0,1]
};

fs.writeFileSync('output/model.json', JSON.stringify(model), 'utf8');
