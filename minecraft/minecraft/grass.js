const { generateDirt } = require('./dirt');
const { getIndexForPosition } = require('../helpers');

const randomInt = (min, max) => (
  Math.floor(Math.random() * (max - min + 1)) + min
);

const generateGrass = (size) => {
  let data = generateDirt(size);
  // Grow grass on top of the dirt
  for(let y = 0; y < size[1]; y++) {
    for(let x = 0; x < size[0]; x++) {
      const depthOfGrass = randomInt(2, 4);
      for(let z = 1; z <= depthOfGrass; z++) {
        const index = getIndexForPosition(size, [x, y, size[2] - z]);
        data[index] = randomInt(1,4);
      }
      const index = getIndexForPosition(size, [x, y, size[2] - depthOfGrass - 1]);
      data[index] = 23;
    }
  }
  return data;
};

const generateGrassBlock = () => generateGrass([16, 16, 16]);

module.exports = {
  generateGrass,
  generateGrassBlock
};
