const crypto = require('crypto');
const fs = require('fs');
const {
  compressData
} = require('./helpers');
const {
  generateGras
} = require('./minecraft/gras');

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16).toUpperCase();
  });
}

const randomInt = (min, max) => (
  Math.floor(Math.random() * (max - min + 1)) + min
);

const size = [32, 32, 16];

const parts = [];
for(let i = 0; i < 10; i++) {
  const data = generateGras(size);
  parts.push({
    name: `frame${i}`,
    bounds: [64,72,55,63,0,1],
    size,
    data: compressData(data),
    uuid: uuidv4(),
    hidden: false
  });
};

const model = {
  formatVersion:2,
  fileType: "SpriteStackModel",
  parts,
  size,
  palette: [5670729,7581794,5539144,5011776,5407046,4747836,5275204,6000205,5670730,5143619,5011777,5077569,6395474,4023092,5275461,8224125,7500402,9276813,6645093,12026713,9726278,7819572,5650465,4879677,12228191,10322504,11701847,6131790,5867851,4748093,5867596,6461524,6329938,5209412,8750469,5802058,7428914,6198096,4681788,5407302,6065998,6659159,4418104,6988891,6922842,5011262,4417848,5011520,5538630,5538888,4945469,6908265,6705195,6197840,6593366,7120989,4286520,6593110,4682044,4483640,5933900,5341254,5736009,5999692,6329680,8289918,5999949,4615996,6263888,4879421,6527317,7450207,6989147,4549178,5143363,4615227,4549690,6461268,6132047,6724951,7492926,5802059,7384416,8019770,4732958],
  bounds: [64,72,55,63,0,1]
};

const SHOW = {
  position: [0,0,0],
  rotation: [0,0,0,1],
  scale: [1,1,1,1]
};

const HIDE = {
  position: [0,0,0],
  rotation: [0,0,0,1],
  scale: [0,0,0,0]
};

const keyframesFromParts = (parts) => (
  parts.map((currentPart, i) => {
    const bones = {};
    if (i === 0) bones.root = SHOW;
    for (const part of parts) {
      console.log(part, currentPart, part === currentPart ? SHOW : HIDE);
      bones[part] = part === currentPart ? SHOW : HIDE;
    }
    return {
      bones,
      index: i
    };
  })
);

const animations = [
  {
    name: "stopmotion",
    length: parts.length,
    uuid: uuidv4(),
    keyframes: keyframesFromParts(parts.map(part => part.name)),
    autoLoop: false
  }
];

fs.writeFileSync('model.json', JSON.stringify(model), 'utf8');
fs.writeFileSync('animations.json', JSON.stringify(animations), 'utf8');
