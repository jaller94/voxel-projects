const { generateDirt } = require('./dirt');
const { getIndexForPosition } = require('../helpers');

const randomInt = (min, max) => (
  Math.floor(Math.random() * (max - min + 1)) + min
);

const generateGras = (size) => {
  let data = generateDirt(size);
  // Grow gras on top of the dirt
  for(let y = 0; y < size[1]; y++) {
    for(let x = 0; x < size[0]; x++) {
      const depthOfGras = randomInt(2, 4);
      for(let z = 1; z <= depthOfGras; z++) {
        const index = getIndexForPosition(size, [x, y, size[2] - z]);
        data[index] = randomInt(1,4);
      }
      const index = getIndexForPosition(size, [x, y, size[2] - depthOfGras - 1]);
      data[index] = 23;
    }
  }
  return data;
};

const generateGrasBlock = () => generateGras([16, 16, 16]);

module.exports = {
  generateGras,
  generateGrasBlock
};
