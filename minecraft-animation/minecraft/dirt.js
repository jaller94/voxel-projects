const randomInt = (min, max) => (
  Math.floor(Math.random() * (max - min + 1)) + min
);

const generateDirt = (size) => {
  const data = [];
  for(let z = 0; z < size[2]; z++) {
    for(let y = 0; y < size[1]; y++) {
      for(let x = 0; x < size[0]; x++) {
        const r = randomInt(0, 255);
        let value = randomInt(20, 23);
        if (r < 4) {
          value = 35;
        } else if (r < 6) {
          value = 52;
        } else if (r < 7) {
          value = 81;
        }
        // 3 times: 35
        // 2 times: 52
        // 1 time : 81
        // other values: 20, 21, 22, 23
        data.push(value);
      }
    }
  }
  return data;
};

module.exports = {
  generateDirt
};
