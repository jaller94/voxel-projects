const {
  compressData,
  uncompressData
} = require('./helpers');

describe('compressData', () => {
  test('keeps individual values in tact', () => {
    const actual = compressData([0,1,2]);
    expect(actual).toStrictEqual([0,1,2]);
  });
  test('compresses two pixels', () => {
    const actual = compressData([0,0]);
    expect(actual).toStrictEqual([-2,0]);
  });
  test('compresses two different values', () => {
    const actual = compressData([0,0,0,1,1,1,1]);
    expect(actual).toStrictEqual([-3,0,-4,1]);
  });
});

describe('umcompressData', () => {
  test('keeps individual values in tact', () => {
    const actual = uncompressData([0,1,2]);
    expect(actual).toStrictEqual([0,1,2]);
  });
  test('uncompresses two pixels', () => {
    const actual = uncompressData([-2,0]);
    expect(actual).toStrictEqual([0,0]);
  });
  test('compresses two different values', () => {
    const actual = uncompressData([-3,0,-4,1]);
    expect(actual).toStrictEqual([0,0,0,1,1,1,1]);
  });
});
