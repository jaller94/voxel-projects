const fillCuboid = (size, input, cuboid, value = 0) => {
  const data = [];
  let index = 0;
  for(let z = 0; z < size[2]; z++) {
    for(let y = 0; y < size[1]; y++) {
      for(let x = 0; x < size[0]; x++) {
        const isPart = (
          x >= cuboid[0][0] && x <= cuboid[1][0] &&
          y >= cuboid[0][1] && y <= cuboid[1][1] &&
          z >= cuboid[0][2] && z <= cuboid[1][2]
        );
        data.push(isPart ? value : input[index]);
        index++;
      }
    }
  }
  return data;
}

const compressData = (data) => {
  if (data.length === 0) return [];
  const compressed = [];
  let index = 1;
  let currentLength = 1;
  let currentValue = data[0];
  while(index < data.length) {
    if (currentValue !== data[index]) {
      if (currentLength > 1) {
        compressed.push(-currentLength);
      }
      compressed.push(currentValue);
      currentLength = 1;
      currentValue = data[index];
    } else {
      currentLength++;
    }
    index++;
  }
  if (currentLength > 1) {
    compressed.push(-currentLength);
  }
  compressed.push(currentValue);
  return compressed;
};

const uncompressData = (data) => {
  const uncompressed = [];
  let index = 0;
  while(index < data.length) {
    if (data[index] < 0) {
      for(let i = 0; i > data[index]; i--) {
        uncompressed.push(data[index + 1]);
      }
      index += 2;
    } else {
      uncompressed.push(data[index]);
      index++;
    }
  }
  return uncompressed;
};

module.exports = {
  compressData,
  fillCuboid,
  uncompressData
};
