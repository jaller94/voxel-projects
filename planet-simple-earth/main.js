const {
  compressData,
  fillCuboid
} = require('./helpers');

const size = [48, 48, 48];
let data = [];

const randomInt = (min, max) => (
  Math.floor(Math.random() * (max - min + 1)) + min
);

for(let z = 0; z<48; z++) {
  for(let y = 0; y<48; y++) {
    for(let x = 0; x<48; x++) {
      const distance = Math.sqrt(
        Math.pow(x - 24, 2) + Math.pow(y - 24, 2) + Math.pow(z - 24, 2)
      );
      if (distance > 22.5) {
        data.push(distance < 24 ? randomInt(11, 12) : 0);
      } else if (distance < 5) {
        data.push(randomInt(28, 29));
      } else {
        data.push(randomInt(3,4))
      }
    }
  }
}

data = fillCuboid(size, data, [[24, 0, 0], [48, 48, 48]]);

const model = {
  formatVersion:2,
  fileType:"SpriteStackModel",
  parts:[
    {
      name:"part1",
      bounds:[64,72,55,63,0,1],
      size,
      data: compressData(data),
      uuid:"E9DB7C60-43B1-4010-B483-FBCA6DCB1D6C",
      hidden:false
    },
  ],
  size,
  palette:[0,2236468,4532284,6699313,9393723,14643494,14262374,15647642,16777215,16511542,10085712,6995504,3642478,4942127,5393188,3292217,4145012,3170434,5992161,6527999,6278628,13360124,10202551,8683143,6908522,5854802,7750282,11285042,14243683,14121914,9410378,9072432],
  bounds:[64,72,55,63,0,1]
};

console.log(JSON.stringify(model));
